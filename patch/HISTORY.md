# update_sequencer_rules.p0.patch

In order to use the sequencer special build rules, we have to modify them
somewhat, most notably in order to:
* Correctly locate the executable `snc`
* Create the require `.dbd` file
* Skip using `mkmf` to create the `.d` file
The `.d` file will be created by the regular build process.

* created by Simon Rose, simon.rose@ess.eu
* Friday, February 10, 2023
