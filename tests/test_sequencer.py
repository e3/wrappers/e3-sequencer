from string import Template

import pytest

from .conftest import SNLWrapper

TEST_SEQ_SRC = Template("""
program test

ss ss1 {
    state init {
        when(delay(1)) {
            printf(${print_text});
        }
        state init
    }
}
""")


@pytest.mark.parametrize("extension", ["st", "stt"])
def test_compile_snl_file(snl_wrapper: SNLWrapper, extension):
    snl_filename = "test_filename"
    seq_source = snl_wrapper.module_dir / f"{snl_filename}.{extension}"
    seq_source.write_text(TEST_SEQ_SRC.substitute(print_text='""'))

    snl_wrapper.add_var_to_module_makefile(
        "SOURCES", f"{snl_filename}.{extension}")

    rc, *_ = snl_wrapper.run_make("cellbuild")
    assert rc == 0

    assert (snl_wrapper.build_dir / f"{snl_filename}.o").is_file()
    assert (snl_wrapper.build_dir / f"{snl_filename}_snl.dbd").is_file()


def test_preprocess_st_file(snl_wrapper: SNLWrapper):
    snl_filename = "test_file.st"
    seq_src = snl_wrapper.module_dir / snl_filename
    seq_src.write_text(
        '#define MESSAGE "waiting\\n"\n' +
        TEST_SEQ_SRC.substitute(print_text="MESSAGE")
    )

    snl_wrapper.add_var_to_module_makefile("SOURCES", snl_filename)

    rc, *_ = snl_wrapper.run_make("cellbuild")
    assert rc == 0


def test_do_not_preprocess_stt_file(snl_wrapper: SNLWrapper):
    snl_filename = "test_file"
    seq_src = snl_wrapper.module_dir / f"{snl_filename}.stt"
    seq_src.write_text(
        '#define MESSAGE "waiting\\n"\n' +
        TEST_SEQ_SRC.substitute(print_text="MESSAGE")
    )

    snl_wrapper.add_var_to_module_makefile("SOURCES", f"{snl_filename}.stt")

    rc, _, errs = snl_wrapper.run_make("cellbuild")
    assert rc == 2
    assert (
        f"No rule to make target `{snl_filename}.c', needed by `{snl_filename}_snl.dbd'"
        in errs
    )
    assert not (snl_wrapper.build_dir / f"{snl_filename}.i").is_file()
