import os
import subprocess

from pathlib import Path
from random import choice
from string import ascii_lowercase

import pytest


class SNLWrapper:
    def __init__(
        self, root_path: Path, **kwargs
    ):
        test_env = os.environ.copy()

        assert "EPICS_BASE" in test_env
        self.epics_base = Path(test_env["EPICS_BASE"])
        self.base_version = self.epics_base.name.split("base-")[-1]

        assert "EPICS_HOST_ARCH" in test_env
        self.host_arch = test_env["EPICS_HOST_ARCH"]

        assert "E3_REQUIRE_VERSION" in test_env
        self.require_version = test_env["E3_REQUIRE_VERSION"]

        e3_require_config = (
            self.epics_base / "require" / self.require_version / "configure"
        )
        assert e3_require_config.is_dir()

        assert "TEMP_CELL_PATH" in test_env
        self.sitemods_path = Path(
            test_env["TEMP_CELL_PATH"]) / f"base-{self.base_version}" / f"require-{self.require_version}"

        self.name = "test_mod_" + "".join(choice(ascii_lowercase)
                                          for _ in range(16))
        self.version = "0.0.0+0"

        self.path = root_path / f"e3-{self.name}"

        self.module_dir = self.path / self.name
        self.module_dir.mkdir(parents=True)

        config_dir = self.path / "configure"
        config_dir.mkdir()

        self.config_module = config_dir / "CONFIG_MODULE"
        self.config_module.touch()

        assert "E3_MODULE_VERSION" in test_env
        self.add_var_to_config_module(
            "SEQUENCER_DEP_VERSION", test_env["E3_MODULE_VERSION"])

        self.build_dir = self.module_dir / \
            f"O.{self.base_version}_{self.host_arch}"

        self.makefile = self.path / "Makefile"
        makefile_contents = f"""
TOP:=$(CURDIR)

E3_MODULE_NAME:={self.name}
E3_MODULE_VERSION:={self.version}
E3_MODULE_SRC_PATH:={self.name}
E3_MODULE_MAKEFILE:={self.name}.Makefile

include $(TOP)/configure/CONFIG_MODULE
-include $(TOP)/configure/CONFIG_MODULE.local

REQUIRE_CONFIG:={e3_require_config}

include $(REQUIRE_CONFIG)/CONFIG
include $(REQUIRE_CONFIG)/RULES_SITEMODS
"""
        self.makefile.write_text(makefile_contents)

        self.module_makefile = self.path / f"{self.name}.Makefile"
        module_makefile_contents = """
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS+=debug
"""
        self.module_makefile.write_text(module_makefile_contents)

    def add_var_to_config_module(self, makefile_var: str, value: str, modifier="+"):
        with open(self.config_module, "a") as f:
            f.write(f"export {makefile_var} {modifier}= {value}\n")

    def add_var_to_module_makefile(self, makefile_var: str, value: str, modifier="+"):
        with open(self.module_makefile, "a") as f:
            f.write(f"{makefile_var} {modifier}= {value}\n")

    def run_make(
        self,
        target: str,
        *args,
        **kwargs,
    ):
        """Attempt to run `make <target> <args>` for the current wrapper."""

        # We should not install or uninstall in the global environment during tests
        if target in ("install", "uninstall"):
            target = f"cell{target}"

        env = os.environ.copy()
        env.update(kwargs)

        args = list(args)
        args.append(f"E3_SITEMODS_PATH={self.sitemods_path}")
        make_cmd = ["make", "-C", self.path, target] + args
        p = subprocess.Popen(
            make_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            env=env,
        )
        outs, errs = p.communicate()
        return p.returncode, outs, errs


@pytest.fixture
def snl_wrapper(tmp_path):
    yield SNLWrapper(tmp_path)
